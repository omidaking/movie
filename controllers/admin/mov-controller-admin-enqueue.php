<?php

/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

class mov_Controller_Admin_Enqueue extends mov_Controller_Admin {


	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {

		$this->register_hook_callbacks();

	}


	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	protected function register_hook_callbacks() {

		mov_Actions_Filters::add_action( 'admin_enqueue_scripts', $this, 'enqueue_styles' );
		mov_Actions_Filters::add_action( 'admin_enqueue_scripts', $this, 'enqueue_scripts' );

	}


	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style(
			mov_Core::mov_ID . '_admin',
			mov_Core::get_mov_url() . 'views/admin/css/mov_admin.css',
			array(),
			mov_Core::mov_VERSION,
			'all'
		);

	}


	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script(
			mov_Core::mov_ID . '_admin',
			mov_Core::get_mov_url() . 'views/admin/js/mov-admin.js',
			array( 'jquery' ),
			mov_Core::mov_VERSION,
			false
		);	
	}
}