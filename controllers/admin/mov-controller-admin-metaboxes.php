<?php
/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

/**
 * takes in a few peices of data and creates a custom meta box
*/
class mov_Controller_Admin_Metaboxes extends mov_Controller_Admin {


	/**
	 * Contains info of metaboxes
	 *
	 * @var null
	 */
	public static $info = null;	


	/**
	 * Contains fields of metaboxes
	 *
	 * @var null
	 */
	public $fields = null;	


	/**
	 * Contains id of metaboxes
	 *
	 * @var string
	 */
	public $id = '';


	/**
	 * Contains title of metaboxes
	 *
	 * @var string
	 */
	public $title = '';


	/**
	 * Contains page of metaboxes
	 *
	 * @var string
	 */
	public $page = '';


	/**
	 * Constructor
	 *
	 * @since    1.0.0
	*/
	protected function __construct() {
		$this->id = '';
		$this->title = '';
		$this->fields = '';
		$this->page = '';
		$this->model = mov_Model_Admin_Metaboxes::get_instance();

	}


	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	public function register_hook_callbacks() {}	


	/**
	 * Loads all info
	 *
	 * @return mixed|null|void
	 */
	public static function register_info() {

		if ( ! is_null( self::$info ) ) {
			return self::$info;
		}

		return self::$info = apply_filters( 'metaboxinfo', array() );
	}	


	/**
	 * Loads all fields
	 *
	 * @return mixed|null|void
	 */
	public static function register_fields() {

		if ( ! is_null( self::$fields ) ) {
			return self::$fields;
		}

		return self::$fields = apply_filters( 'metaboxfields', array() );
	}


	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	public function init( $id, $page, $title, $fields ) {

		$info = static::register_info();

		$this->id = $id;
		$this->title = $title;
		$this->fields = $fields;
		$this->page = $page;
		if( ! is_array( $this->page ) )
			$this->page = array( $this->page );
		
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
		add_action( 'admin_head',  array( $this, 'admin_head' ) );
		add_action( 'add_meta_boxes', array( $this, 'add_box' ) );
		add_action( 'save_post',  array( $this, 'save_box' ));
	}	


	/**
	 * enqueue necessary scripts and styles
	 */
	function admin_enqueue_scripts() {
		global $pagenow;

		if ( in_array( $pagenow, array( 'post-new.php', 'post.php' ) ) && in_array( get_post_type(), $this->page ) ) {
			// js
			$deps = array( 'jquery' );
			$deps[] = 'jquery-ui-sortable';
			if ( static::get_model()->meta_box_find_field_type( 'date', $this->fields ) )
				$deps[] = 'jquery-ui-datepicker';
			if ( static::get_model()->meta_box_find_field_type( 'slider', $this->fields ) )
				$deps[] = 'jquery-ui-slider';
			if ( static::get_model()->meta_box_find_field_type( 'color', $this->fields ) )
				$deps[] = 'farbtastic';
			if ( in_array( true, array(
				static::get_model()->meta_box_find_field_type( 'chosen', $this->fields ),
				static::get_model()->meta_box_find_field_type( 'post_chosen', $this->fields )
			) ) ) {
				wp_register_script( 'chosen', mov_Core::get_mov_url() . 'views/admin/js/chosen.js' , array( 'jquery' ) );
				$deps[] = 'chosen';
				wp_enqueue_style( 'chosen',  mov_Core::get_mov_url() . 'views/admin/css/chosen.css' );
			}
			if ( in_array( true, array( 
				static::get_model()->meta_box_find_field_type( 'date', $this->fields ), 
				static::get_model()->meta_box_find_field_type( 'slider', $this->fields ),
				static::get_model()->meta_box_find_field_type( 'color', $this->fields ),
				static::get_model()->meta_box_find_field_type( 'chosen', $this->fields ),
				static::get_model()->meta_box_find_field_type( 'post_chosen', $this->fields ),
				static::get_model()->meta_box_find_repeatable( 'repeatable', $this->fields ),
				static::get_model()->meta_box_find_field_type( 'image', $this->fields ),
				static::get_model()->meta_box_find_field_type( 'file', $this->fields )
			) ) )
				wp_enqueue_script( 'meta_box', mov_Core::get_mov_url() . 'views/admin/js/scripts.js' , $deps );

			// css
				$deps = array();
				wp_register_style( 'jqueryui', mov_Core::get_mov_url() . 'views/admin/css/jqueryui.css' );
				if ( static::get_model()->meta_box_find_field_type( 'date', $this->fields ) || static::get_model()->meta_box_find_field_type( 'slider', $this->fields ) )
					$deps[] = 'jqueryui';
				if ( static::get_model()->meta_box_find_field_type( 'color', $this->fields ) )
					$deps[] = 'farbtastic';
				wp_enqueue_style( 'meta_box', mov_Core::get_mov_url() . 'views/admin/css/meta_box.css' , $deps );
			}
		}

	/**
	 * adds scripts to the head for special fields with extra js requirements
	 */
	function admin_head() {
		if ( in_array( get_post_type(), $this->page ) && ( static::get_model()->meta_box_find_field_type( 'date', $this->fields ) || static::get_model()->meta_box_find_field_type( 'slider', $this->fields ) ) ) {

			echo '<script type="text/javascript">
			jQuery(function( $) {';
			
			foreach ( $this->fields as $field ) {
				switch( $field['type'] ) {
					// date
					case 'date' :
					echo '$("#' . $field['id'] . '").datepicker({
						dateFormat: \'yy-mm-dd\'
					});';
					break;
					// slider
					case 'slider' :
					$value = get_post_meta( get_the_ID(), $field['id'], true );
					if ( $value == '' )
						$value = $field['min'];
					echo '
					$( "#' . $field['id'] . '-slider" ).slider({
						value: ' . $value . ',
						min: ' . $field['min'] . ',
						max: ' . $field['max'] . ',
						step: ' . $field['step'] . ',
						slide: function( event, ui ) {
							$( "#' . $field['id'] . '" ).val( ui.value );
						}
					});';
					break;
				}
			}
			
			echo '});
			</script>';

		}
	}
	
	/**
	 * adds the meta box for every post type in $page
	 */
	function add_box() {
		foreach ( $this->page as $page ) {
			add_meta_box( $this->id, $this->title, array( $this, 'meta_box_callback' ), $page, 'normal', 'high' );
		}
	}
	
	/**
	 * outputs the meta box
	 */
	function meta_box_callback() {
		// Use nonce for verification
		wp_nonce_field( 'custom_meta_box_nonce_action', 'custom_meta_box_nonce_field' );
		
		// Begin the field table and loop
		echo '<table class="form-table meta_box">';
		foreach ( $this->fields as $field) {
			if ( $field['type'] == 'section' ) {
				echo '<tr>
				<td colspan="2">
				<h2>' . $field['label'] . '</h2>
				</td>
				</tr>';
			}
			else {
				echo '<tr>
				<th style="width:20%"><label for="' . $field['id'] . '">' . $field['label'] . '</label></th>
				<td>';

				$meta = get_post_meta( get_the_ID(), $field['id'], true);

				echo static::get_model()->custom_meta_box_field( $field, $meta );

				echo     '<td>
				</tr>';
			}
		} // end foreach
		echo '</table>'; // end table
	}
	
	/**
	 * saves the captured data
	 */
	function save_box( $post_id ) {
		$post_type = get_post_type();
		
		// verify nonce
		if ( ! isset( $_POST['custom_meta_box_nonce_field'] ) )
			return $post_id;
		if ( ! ( in_array( $post_type, $this->page ) || wp_verify_nonce( $_POST['custom_meta_box_nonce_field'],  'custom_meta_box_nonce_action' ) ) ) 
			return $post_id;
		// check autosave
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return $post_id;
		// check permissions
		if ( ! current_user_can( 'edit_page', $post_id ) )
			return $post_id;
		
		// loop through fields and save the data
		foreach ( $this->fields as $field ) {
			if( $field['type'] == 'section' ) {
				$sanitizer = null;
				continue;
			}
			if( in_array( $field['type'], array( 'tax_select', 'tax_checkboxes' ) ) ) {
				// save taxonomies
				if ( isset( $_POST[$field['id']] ) ) {
					$term = $_POST[$field['id']];
					wp_set_object_terms( $post_id, $term, $field['id'] );
				}
			} else {
				// save the rest
				$new = false;
				$old = get_post_meta( $post_id, $field['id'], true );
				if ( isset( $_POST[$field['id']] ) )
					$new = $_POST[$field['id']];
				if ( isset( $new ) && '' == $new && $old ) {
					delete_post_meta( $post_id, $field['id'], $old );
				} elseif ( isset( $new ) && $new != $old ) {
					$sanitizer = isset( $field['sanitizer'] ) ? $field['sanitizer'] : 'sanitize_text_field';
					if ( is_array( $new ) ) {
						$new = static::get_model()->meta_box_array_map_r( 'self::meta_box_sanitize', $new, $sanitizer );
					}
					else {
						$new = static::get_model()->meta_box_sanitize( $new, $sanitizer );
					}
					update_post_meta( $post_id, $field['id'], $new );
				}
			}
		} // end foreach
	}
	
}

