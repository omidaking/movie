<?php
/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */
class mov_Controller_Admin_Metafields extends mov_Controller_Admin {

	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {

		$this->register_hook_callbacks();

	}

	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	public function register_hook_callbacks() {

		$this->movie();
		$this->shortcode_genrator();
		    // add_action( 'admin_footer', array( $this, 'test' ) );
	}

	public function test(){


		var_dump( get_option( 's' ) );

	}


	public function movie() {

		$info = array(
			'id' => 'Actors Information',
			'page' => 'movie',
			'title' => 'Actors Information'
		);


		$prefix = 'movie';
		$fields = array(
			array(
				'label'	=> 'Sorting number',
				'desc'	=> 'Assign number for demonstrate this movie erlier than others.',
				'id'	=> $prefix.'text',
				'type'	=> 'number'
			)
		);


		$metam = new mov_Controller_Admin_Metaboxes;
		$metam->init( $info['id'], $info['page'], $info['title'], $fields );

	}

	// public function metbox_info( $info ){


	// }

	// public function movie_add_customfields( $fields ) {


	// }

	public function shortcode_genrator() {

		// add_filter( 'metaboxinfo', array( $this, 'shortcode_genrator_metabox_info' ) );
		// add_filter( 'metaboxfields', array( $this, 'shortcode_genrator_add_customfields' ) );
		$info = array(
			'id' => 'Shortcode Configuration',
			'page' => 'movie_genrator',
			'title' => 'Shortcode Configuration'
		);


		$prefix = 'movie_genrator';

		$fields = array(
			array (
				'label'	=> 'Select Genres', 
				'desc'	=> 'Select desired geners to demonstrate on front end', 
				'id'	=> $prefix.'checkbox_group', 
				'type'	=> 'checkbox_group', 
				'options' => array (
					'term'	=> 'genre' 
				)
			),
		);

		$ddd = new mov_Controller_Admin_Metaboxes;
		$ddd->init( $info['id'], $info['page'], $info['title'], $fields ) ;

	}


	// public function shortcode_genrator_metabox_info( $info ){


	// }

	// public function shortcode_genrator_add_customfields( $fields ) {


	// }

}

	
