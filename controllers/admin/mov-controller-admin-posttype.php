<?php
/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

/**
* Register post type for movie
*/
class mov_Controller_Admin_Posttype extends mov_Controller_Admin {


	/**
	 * Constructor
	 *
	 * @since    1.0.0
	*/
	protected function __construct() {

	 	$this->register_hook_callbacks();

	}


	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	public function register_hook_callbacks() {

		$this->register_movie_pt();
		$this->register_shortcode_genrator();
		add_filter( 'manage_movie_genrator_posts_columns', array( $this, 'wb_shortcode_show' ) );
		add_action( 'manage_movie_genrator_posts_custom_column', array( $this, 'wb_shortcode_content_show' ), 10, 2);
	}	


	/**
	 * Create Movie post type
	 *
	 * @since    1.0.0
	 */
	public function register_movie_pt() {

		$pt_name = array (
			'post_type_name' => 'movie',
			'plural' => 'Movies',
			'singular' => 'Movie'
		);

		$settings = array(
			'rewrite' => array( 'slug' => 'movie' ),
			'menu_position' => null,
			'menu_icon'			 => 'dashicons-format-video',
			'supports' => array( 'title', 'editor', 'thumbnail' )
		);

		$ptclass = new mov_Model_Admin_Posttype;
		$ptclass->add( $pt_name, $settings );

	}

	/**
	 * Create shortcode genrator post type
	 *
	 * @since    1.0.0
	 */
	public function register_shortcode_genrator() {

		$pt_name = array (
			'post_type_name' => 'movie_genrator',
			'plural' => 'Shortcodes Genrator',
			'singular' => 'Shortcode Genrator'
		);

		$settings = array(
			'rewrite'            => array( 'slug' => 'movie_genrator' ),
			'menu_position'      => null,
			'show_ui'			 => true,
			'show_in_menu'		 => 'edit.php?post_type=movie',
			'supports'           => array( 'title' )
		);

		$ptclass = new mov_Model_Admin_Posttype;
		$ptclass->add( $pt_name, $settings );
	}


	/**
     * Add shortcode col
     */
	function wb_shortcode_show( $defaults ) {
		$defaults['shortcode'] = 'Short Code';
		return $defaults;
	}

	/**
     * Add Shortcode content
     */
	function wb_shortcode_content_show( $column_name, $post_ID ){
		echo '[movie id='."$post_ID".']';
	}

}
