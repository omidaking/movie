<?php
/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

/**
* Register post type for movie
*/
class mov_Controller_Admin_Taxonomies extends mov_Controller_Admin {


	/**
	 * Constructor
	 *
	 * @since    1.0.0
	*/
	protected function __construct() {

		$this->register_hook_callbacks();
	}


	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	public function register_hook_callbacks() {

		$this->register_movie_tx_genre();
		$this->register_movie_tx_actor();
	}


	/**
	 * Create taxonomy genre
	 *
	 * @since    1.0.0
	 */
	public function register_movie_tx_genre() {

		$settings = array(
			'hierarchical' => true
		);

		$tx =  new mov_Model_Admin_Taxonomies;
		$tx->add( 'genre', 'movie', $settings );
	}


	/**
	 * Create taxonomy actor
	 *
	 * @since    1.0.0
	 */
	public function register_movie_tx_actor() {

		$settings = array(
			'hierarchical' => true
		);

		$tx =  new mov_Model_Admin_Taxonomies;
		$tx->add( 'actor', 'movie', $settings );
	}

}
