<?php

/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

abstract class mov_Controller_Admin extends mov_Controller {

	/**
	 * Render a template
	 *
	 * @since    1.0.0
	 */
	protected static function render_template( $default_template_path = false, $variables = array(), $require = 'once' ){

		if ( ! $template_path = locate_template( basename( $default_template_path ) ) ) {
			$template_path = mov_Core::get_mov_path() . '/views/admin/' . $default_template_path;
		}

		if ( is_file( $template_path ) ) {
			extract( $variables );
			ob_start();
			if ( 'always' == $require ) {
				require( $template_path );
			} else {
				require_once( $template_path );
			}
			$template_content = apply_filters( 'mov_template_content', ob_get_clean(), $default_template_path, $template_path, $variables );
		} else {
			$template_content = '';
		}

		return $template_content;
	}

}