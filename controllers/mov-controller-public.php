<?php

/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

class mov_Controller_Public extends mov_Controller {

	private static $nounce  = 'movie_security';


	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {

		$this->register_hook_callbacks();
		$this->user_setting = $this->get_user_settings();
	}


	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	protected function register_hook_callbacks() {
		// set scripts and styles
		mov_Actions_Filters::add_action( 'wp_enqueue_scripts', $this, 'enqueue_styles', 9999 );
		mov_Actions_Filters::add_action( 'wp_enqueue_scripts', $this, 'enqueue_scripts' );
	}


	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style(
			mov_Core::mov_ID,
			mov_Core::get_mov_url() . 'views/css/mov.css',
			array(),
			mov_Core::mov_VERSION,
			'all'
		);		
	}


	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script(
			mov_Core::mov_ID,
			mov_Core::get_mov_url() . 'views/js/mov.js',
			array( 'jquery' ),
			mov_Core::mov_VERSION,
			false
		);

		$this->loclize();
	}


	/**
	 * Define some loclization to js.
	 *
	 * @since    1.0.0
	 */
	public function loclize(){

		$nounce = wp_create_nonce( self::$nounce );
		wp_localize_script( mov_Core::mov_ID , 'mov', array(
			'adminajax'         => admin_url( 'admin-ajax.php' ),
			'nounce'     		=> $nounce
		));

	}	

	/**
	 * Get current language of WordPress
	 *
	 * @return string
	 */
	public function get_current_language() {
		return apply_filters('plugin_locale', get_locale(), 'mov');
	}

}