<?php

/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

class mov_Controller_Public_Movie_Filter extends mov_Controller_Public {

	public $ajax_action  = 'movie';
	private static $nounce  = 'movie_security';

	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {
		// get user setting
		$this->user_setting = $this->get_user_settings();

 	}

	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	public function register_hook_callbacks(){}


	/**
	 * Ajax callback for filter
	 *
	 * @since    1.0.0
	 */
	public function render(){
		// check security nounce
		check_ajax_referer( self::$nounce, 'security' );
		$_POST['k'] = isset( $_POST['k'] ) ? $_POST['k'] : '';
		
		// create argumuns for query
		$args = array(
			'post_type' => 'movie',
			'orderby'   => 'meta_value_num',
			'order'     => 'ASC',
			'tax_query' => array(
				array(
					'taxonomy' => $_POST['type'] ,
					'field'    => 'slug',
					'terms'    =>  $_POST['value'] 
				),
			),
			'meta_query' => array(
				array(
					'key'     => 'movietext',
					'value'   => array( 0, 90000 ),
					'type'    => 'numeric',
					'compare' => 'BETWEEN',
				),
			),
		);

		$out = '';
		// the query
		$the_query = new WP_Query( $args );
		// start post loop
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				$term = $actors = $actor = $terms = '';
				if ( $_POST['k'] !== 'widget' ) {
					$out .=  '<div class="movie-inner-wrap col-md-6">
								<div class="movie-content">';
				}
				// get content
				$content = get_the_content();

				// get abstract of the content
				preg_match( '/^([^.!?\s]*[\.!?\s]+){0,25}/', strip_tags( $content ), $abstract );

				// gey terms
				$term = wp_get_post_terms( get_the_ID(), 'genre', array("fields" => "all") );
				foreach ( $term as $key => $value ) {
					$terms .= $value->name . ', ';
				}

				// get actors
				$actors = get_post_meta( get_the_ID(), 'movierepeatable', true );
				foreach ( $actors as $key => $value ) {
					$actor .= $value['name'] . ', ';
				}
				if ( $_POST['k'] === 'widget' ) {
					$out = '
						<div class="movie-widget-inner">
							<figure>
								' . get_the_post_thumbnail( get_the_ID(), 'thumbnail' ) . '
							</figure>
							<h6> ' . get_the_title() . '</h6>
						</div>';
				} else {
					$out .= '
						<figure>
							' . get_the_post_thumbnail( get_the_ID(), 'large' ) . '
						</figure>
						<h4> ' . get_the_title() . '</h4>
						<h6>' . rtrim( $terms , ', ' ) .'</h6>
						<p>' . $abstract[0] . ' ...' . '</p>
						<span>' . rtrim( $actor , ', ' ) .'</span>
						';
					$out .=  '</div>
					</div>';
				}

			}
			wp_reset_postdata();
		} else {
			$out = 'No movie found!';
		}

		wp_send_json( $out );
	}

}
