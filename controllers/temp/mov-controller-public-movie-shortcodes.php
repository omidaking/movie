<?php

/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

class mov_Controller_Public_Movie_Shortcodes extends mov_Controller_Public {


	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {

		add_shortcode( 'movie', array( $this, 'shortcode' ) );
		// get user setting
		$this->user_setting = $this->get_user_settings();

 	}


	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	public function register_hook_callbacks(){}


	/**
	 * Create shortcode
	 *
	 * @since    1.0.0
	 */
	function shortcode( $atts, $content ){

		$user_id = shortcode_atts(array( 
			'id' => 'NULL'), $atts);
		$loop = array();
		$post_id = $user_id['id'];

		$meta = get_post_meta( $post_id, 'movie_genratorcheckbox_group', true );


		foreach ( $meta as $key => $value) {
			$loop[] =  $value;
		}

		$args = array(
		'post_type' => 'movie',
		'orderby'   => 'meta_value_num',
		'order'     => 'ASC',
			'tax_query' => array(
				array(
					'taxonomy' => 'genre',
					'field'    => 'slug',
					'terms'    =>  $loop 
				),
			),
			'meta_query' => array(
				array(
					'key'     => 'movietext',
					'value'   => array( 0, 90000 ),
					'type'    => 'numeric',
					'compare' => 'BETWEEN',
				),
			),
		);


		// the query

		$out = '<div class="mov-preloader-wrap">
					<div class="mov-preloader"></div>
				</div>';

		// search bar and wrap content
		$out .=  '<div class="movie-wrap col-md-12">
					<div class="movie-search-bar">
						<select>';
						foreach ( $meta as $key => $value ) {
							$out .= '<option data-type="genre" value="'.$value.'">'. $value .'</option>';
						}
		$out .=  	'	</select>
						<select>';
						$actors = get_terms( 'actor' , 'get=all');
						foreach ( $actors as $key => $value ) {
							$out .= '<option data-type="actor" value="'.$value->slug.'">'. $value->name .'</option>';
						}
		$out .= 		
					'	</select>
					</div>';

		// the query
		$the_query = new WP_Query( $args );
		// start post loop
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				$actors = $terms = '';
			$out .=  '<div class="movie-inner-wrap col-md-6">
						<div class="movie-content">';
				// get content
				$content = get_the_content();

				// get abstract of the content
				preg_match( '/^([^.!?\s]*[\.!?\s]+){0,25}/', strip_tags( $content ), $abstract );

				// gey terms
				$term = wp_get_post_terms( get_the_ID(), 'genre', array("fields" => "all") );
				foreach ( $term as $key => $value ) {
					$terms .= $value->name . ', ';
				}

				// get actors
				$actor = wp_get_post_terms( get_the_ID(), 'actor', array("fields" => "all") );
				foreach ( $actor as $key => $value ) {
					$actors .= $value->name .', ';
				}

				$out .= '
						<figure>
							' . get_the_post_thumbnail( get_the_ID(), 'large' ) . '
						</figure>
						<h4> ' . get_the_title() . '</h4>
						<h6>' . rtrim( $terms , ', ' ) .'</h6>
						<p>' . $abstract[0] . ' ...' . '</p>

						<span>' . rtrim( $actors , ', ' ) .'</span>
						';
			$out .=  '</div>
					</div>';
			}
			wp_reset_postdata();
		} else {
			$out = '<div class="movie-inner-wrap col-md-6"> ' . __( "No movie found!", "mov" );
		}


		$out .=  '</div>';

		// return
		return $out;

	}

}
