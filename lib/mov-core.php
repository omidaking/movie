<?php

/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

class mov_Core {

	/**
	 * mov Instance
	 */
	private static $instance;

	/**
	 * The modules variable holds all modules of the mov.
	 */
	private static $modules = array();

	/**
	 * Main plugin path.
	 */
	private static $mov_path;

	/**
	 * Absolute plugin url.
	 */
	private static $mov_url;


	/**
	 * The unique identifier of this mov.
	 */
	const mov_ID = 'movie-plugin';

	/**
	 * The name identifier of this mov.
	 */
	const mov_NAME = 'Movie';


	/**
	 * The current version of the mov.
	 */
	const mov_VERSION = '1.0.0';

	/**
	 * The mov prefix to referenciate classes inside the mov
	 */
	const CLASS_PREFIX = 'mov_';

	/**
	 * The mov prefix to referenciate files and prefixes inside the mov
	 */
	const mov_PREFIX = 'mov-';
	
	/**
	 * Provides access to a single instance of a module using the singleton pattern
	 */
	public static function get_instance() {

		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;

	}

	/**
	 * Define the core functionality of the mov.
	 */
	public function __construct() {

		self::$mov_path = plugin_dir_path( dirname( __FILE__ ) );
		self::$mov_url  = plugin_dir_url( dirname( __FILE__ ) );
		require_once( self::$mov_path . 'lib/' . self::mov_PREFIX . 'loader.php' );

		self::$modules['mov_Loader']						= mov_Loader::get_instance();
		self::$modules['mov_Controller_Public']            	= mov_Controller_Public::get_instance();
		self::$modules['mov_Controller_Admin_Enqueue']     	= mov_Controller_Admin_Enqueue::get_instance();
		self::$modules['mov_Controller_Admin_Notices']     	= mov_Controller_Admin_Notices::get_instance();
		self::$modules['mov_Model_Public_Ajax']       		= mov_Model_Public_Ajax::get_instance();
		self::$modules['mov_Controller_Admin_Posttype']     = mov_Controller_Admin_Posttype::get_instance();
		self::$modules['mov_Controller_Admin_Taxonomies']   = mov_Controller_Admin_Taxonomies::get_instance();
		self::$modules['mov_Controller_Admin_Metafields']   = mov_Controller_Admin_Metafields::get_instance();
		self::$modules['mov_Controller_Public_Movie_Shortcodes']  = mov_Controller_Public_Movie_Shortcodes::get_instance();

		mov_Actions_Filters::init_actions_filters();

	}

	/**
	 * Get plugin's absolute path.
	 */
	public static function get_mov_path() {

		return isset( self::$mov_path ) ? self::$mov_path : plugin_dir_path( dirname( __FILE__ ) );

	}

	/**
	 * Get plugin's absolute url.
	 */
	public static function get_mov_url() {

		return isset( self::$mov_url ) ? self::$mov_url : plugin_dir_url( dirname( __FILE__ ) );

	}

	public static function get_option( $option, $section, $default = '' ) {
		if ( empty( $option ) )
			return;
		$options = get_option( $section );
		if ( isset( $options[$option] ) ) {
			return $options[$option];
		}
		return $default;
	}

}