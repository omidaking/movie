<?php

/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

class mov_Loader {

	/**
	 * mov Instance
	 */
	private static $instance;


	/**
	 * mov Instance
	 */
	public $name;

	/**
	 * Provides access to a single instance of a module using the singleton pattern
	 */
	public static function get_instance() {

		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;

	}

	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {

		spl_autoload_register( array( $this, 'mov_load' ) );

		$this->set_locale();
		$this->register_hook_callbacks();

	}

	public function __get( $var ) {
		return $this->name = $var;
	}

	/**
	 * Loads all Plugin dependencies
	 *
	 * @since    1.0.0
	 */
	private function mov_load( $class ) {

		if ( false !== strpos( $class, mov_Core::CLASS_PREFIX ) ) {

			$cname = str_replace( '_', '-', strtolower( $class ) ) . '.php';
			$folder        = '/';

			if ( false !== strpos( $class, '_Admin' ) ) {
				$folder .= 'admin/';
			}			

			if ( false !== strpos( $class, '_Movie' ) ) {
				$folder .= 'temp/';
			}			

			if ( false !== strpos( $class, mov_Core::CLASS_PREFIX . 'Controller' ) ) {
				$path = mov_Core::get_mov_path() . 'controllers' . $folder . $cname;
				require_once( $path );
			} elseif ( false !== strpos( $class, mov_Core::CLASS_PREFIX . 'Model' ) ) {
				$path = mov_Core::get_mov_path() . 'models' . $folder . $cname;
				require_once( $path );
			} else {
				$path = mov_Core::get_mov_path() . 'lib/' . $cname;
				require_once( $path );
			}
			require_once( mov_Core::get_mov_path() . 'models/admin/mov-model-admin-widget.php' );

		}

	}

	/**
	 * Define the locale for mov for internationalization.
	 *
	 * @since    1.0.0
	 */
	private function set_locale() {

		$plugin_i18n = new mov_i18n();
		$plugin_i18n->set_domain( 'mov' );

		mov_Actions_Filters::add_action( 'plugins_loaded', $plugin_i18n, 'textdomain' );

	}

	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	public function register_hook_callbacks() {

		// register_activation_hook(   mov_Core::get_mov_path() . 'movie.php', array( $this, 'activate' ) );
		// register_deactivation_hook( mov_Core::get_mov_path() . 'movie.php', array( $this, 'deactivate' ) );

	}	


	/**
	 * Prepares sites to use the plugin during single or network-wide activation
	 *
	 * @since    1.0.0
	 * @param bool $network_wide
	 */
	public function activate() {}
	
	/**
	 * Rolls back activation procedures when de-activating the plugin
	 *
	 * @since    1.0.0
	 */
	public function deactivate() {

		mov_Model_Admin_Notices::remove_admin_notices();

	}

	/**
	 * Fired when user uninstalls the plugin, called in unisntall.php file
	 *
	 * @since    1.0.0
	 */
	public static function uninstall_plugin() {

	}

}
