<?php

class mov_Request_Validator
{
	private $action;
	private $name;

	public function __construct() {
		// $this->register_hook_callbacks();
	}

	public function register_hook_callbacks() {
		add_action( 'init', array( $this, 'is_user_logged_in' ) );
	}	

	public function get_nonce_name() {
		return $this->name;
	}

	public function get_nonce_action() {
		return $this->action;
	}

	public function is_valid( $ajax = FALSE ) {
		return wp_verify_nonce( $_REQUEST[ $this->name ], $this->action );
	}

	public static function is_ajax() {

		return defined( 'DOING_AJAX' ) && DOING_AJAX;

	}

	public function is_registered() {

		$user = wp_get_current_user();
		
		return $user->exists();

	}


	public function must_sanitize( &$data , $type ) {

		$data = wp_filter_nohtml_kses( $data );

		switch ( $type ){
			case 'user':
				$data = sanitize_user( $data ); 
			return $data;				

			case 'pass':
				$data = wp_filter_nohtml_kses( $data );
			return $data;				

			case 'text':
				$data = sanitize_text_field( urldecode( $data ) );
			return $data;				

			case 'email':
				$data = sanitize_email( urldecode( $data ) );
			return $data;			
		}

	}	

	public function must_serilize( $data ) {

		$get = explode('&', $data ); // explode with and

		foreach ( $get as $key => $value) {
			$need[ substr( $value, 0 , strpos( $value, '=' ) ) ] =  substr( $value, strpos( $value, '=' ) + 1 ) ;
		}
		return $need;
	}

	public function terms_empty( $data ) {

		if ( sanitize_text_field( $data ) == ' ' || empty( $data ) ) return -1; else return 0;

	}	

	public static function is_numeric( $data , $type ) {
		if ( $type == true ) {
			if ( !is_numeric( intval( $data ) ) ) wp_die( 'Id Not Valid.' );
		} else {
			if ( is_numeric( intval( $data ) ) ) return $data; else return false;
		}

	}

	public function is_user_logged_in() {
		$user = wp_get_current_user();

		if ( empty( $user->ID ) )
			return false;

		return true;
	}

}