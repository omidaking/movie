<?php


class mov_Settings {

	private $settings;

	public function __construct() {

		$this->register_hook_callbacks();

	}

	public function register_hook_callbacks() {

		mov_Actions_Filters::add_action( 'plugins_loaded', $this, 'get_user_option' );

	}

	public function get_user_option() {

		$this->settings['memberfilter'] = mov_Core::get_option( 'dc_enable_filter_member', 'Comment_Filter_Bar' );

		return $this->settings; 

	}

}