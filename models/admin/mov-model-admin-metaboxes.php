<?php

/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

/**
* Register taxonomies class
*/
class mov_Model_Admin_Metaboxes extends mov_Model_Admin {


	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	public function __construct() { }


	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	public function register_hook_callbacks() {}	


	/**
	 * recives data about a form field and spits out the proper html
	 *
	 * @return	string
	 */
	public static function custom_meta_box_field( $field, $meta = null, $repeatable = null ) {
		if ( ! ( $field || is_array( $field ) ) )
			return;
		
		// get field data
		$type = isset( $field['type'] ) ? $field['type'] : null;
		$label = isset( $field['label'] ) ? $field['label'] : null;
		$desc = isset( $field['desc'] ) ? '<span class="description">' . $field['desc'] . '</span>' : null;
		$place = isset( $field['place'] ) ? $field['place'] : null;
		$size = isset( $field['size'] ) ? $field['size'] : null;
		$post_type = isset( $field['post_type'] ) ? $field['post_type'] : null;
		$options = isset( $field['options'] ) ? $field['options'] : null;
		$settings = isset( $field['settings'] ) ? $field['settings'] : null;
		$repeatable_fields = isset( $field['repeatable_fields'] ) ? $field['repeatable_fields'] : null;


		// the id and name for each field
		$id = $name = isset( $field['id'] ) ? $field['id'] : null;
		if ( $repeatable ) {
			$name = $repeatable[0] . '[' . $repeatable[1] . '][' . $id .']';
			$id = $repeatable[0] . '_' . $repeatable[1] . '_' . $id;
		}


		switch( $type ) {
			// basic
			case 'text':
			case 'tel':
			case 'email':
			default:
				echo '<input type="' . $type . '" name="' . esc_attr( $name ) . '" id="' . esc_attr( $id ) . '" value="' . esc_attr( $meta ) . '" class="regular-text" size="30" />
						<br />' . $desc;
			break;

			// checkbox_group
			case 'checkbox_group':
				echo '<ul class="meta_box_items">';
				$terms = get_terms( $options['term'] , 'get=all');
				$loop = array();
				foreach ( $terms as $key => $value) {
					$loop[] = array ( 
						'label' => $value->name, 
						'value'	=> $value->slug 
					);
				}

				foreach ( $loop as $option )
					echo '<li><input type="checkbox" value="' . $option['value'] . '" name="' . esc_attr( $name ) . '[]" id="' . esc_attr( $id ) . '-' . $option['value'] . '"' , is_array( $meta ) && in_array( $option['value'], $meta ) ? ' checked="checked"' : '' , ' /> 
							<label for="' . esc_attr( $id ) . '-' . $option['value'] . '">' . $option['label'] . '</label></li>';
				echo '</ul>' . $desc;
			break;

			case 'number':
				echo '<input type="' . $type . '" name="' . esc_attr( $name ) . '" id="' . esc_attr( $id ) . '" value="' . intval( $meta ) . '" class="regular-text" size="30" />
						<br />' . $desc;
			break;

			// repeatable
			case 'repeatable':
				echo '<table id="' . esc_attr( $id ) . '-repeatable" class="meta_box_repeatable" cellspacing="0">
					<thead>
						<tr>
							<th><span class="sort_label"></span></th>
							<th>Fields</th>
							<th><a class="meta_box_repeatable_add" href="#"></a></th>
						</tr>
					</thead>
					<tbody>';

				$i = 0;
				// create an empty array
				if ( $meta == '' || $meta == array() ) {
					$keys = wp_list_pluck( $repeatable_fields, 'id' );
					$meta = array ( array_fill_keys( $keys, null ) );
				}
				$meta = array_values( $meta );
				foreach( $meta as $row ) {

					echo '<tr>
							<td><span class="sort hndle"></span></td><td>';
					foreach ( $repeatable_fields as $repeatable_field ) {
						if ( ! array_key_exists( $repeatable_field['id'], $meta[$i] ) )
							$meta[$i][$repeatable_field['id']] = null;
						echo '<label>' . $repeatable_field['label']  . '</label><p>';
						echo self::custom_meta_box_field( $repeatable_field, $meta[$i][$repeatable_field['id']], array( $id, $i ) );
						echo '</p>';
					} // end each field
					echo '</td><td><a class="meta_box_repeatable_remove" href="#"></a></td></tr>';
					$i++;
				} // end each row
				echo '</tbody>';
				echo '
					<tfoot>
						<tr>
							<th><span class="sort_label"></span></th>
							<th>Fields</th>
							<th><a class="meta_box_repeatable_add" href="#"></a></th>
						</tr>
					</tfoot>';
				echo '</table>
					' . $desc;
			break;
		} //end switch
			
	}


	/**
	 * Finds any item in any level of an array
	 *
	 * @return	bool
	 */
	public static function meta_box_find_field_type( $needle, $haystack ) {
		foreach ( $haystack as $h )
			if ( isset( $h['type'] ) && $h['type'] == 'repeatable' )
				return self::meta_box_find_field_type( $needle, $h['repeatable_fields'] );
			elseif ( ( isset( $h['type'] ) && $h['type'] == $needle ) || ( isset( $h['repeatable_type'] ) && $h['repeatable_type'] == $needle ) )
				return true;
		return false;
	}


	/**
	 * Find repeatable
	 *
	 * @return	bool
	 */
	public static function meta_box_find_repeatable( $needle = 'repeatable', $haystack ) {
		foreach ( $haystack as $h )
			if ( isset( $h['type'] ) && $h['type'] == $needle )
				return true;
		return false;
	}


	/**
	 * sanitize boolean inputs
	 */
	function meta_box_santitize_boolean( $string ) {
		if ( ! isset( $string ) || $string != 1 || $string != true )
			return false;
		else
			return true;
	}


	/**
	 * outputs properly sanitized data
	 *
	 * @return
	 */
	public static function meta_box_sanitize( $string, $function = 'sanitize_text_field' ) {
		switch ( $function ) {
			case 'intval':
				return intval( $string );
			case 'absint':
				return absint( $string );
			case 'wp_kses_post':
				return wp_kses_post( $string );
			case 'wp_kses_data':
				return wp_kses_data( $string );
			case 'esc_url_raw':
				return esc_url_raw( $string );
			case 'is_email':
				return is_email( $string );
			case 'sanitize_title':
				return sanitize_title( $string );
			case 'santitize_boolean':
				return santitize_boolean( $string );
			case 'sanitize_text_field':
			default:
				return sanitize_text_field( $string );
		}
	}


	/**
	 * Map a multideminsional array
	 *
	 * @return	array
	 */
	public static function meta_box_array_map_r( $func, $meta, $sanitizer ) {
		$newMeta = array();
		$meta = array_values( $meta );
		
		foreach( $meta as $key => $array ) {
			if ( $array == '' )
				continue;

			if ( ! is_array( $array ) ) {
				return array_map( $func, $meta, (array)$sanitizer );
				break;
			}

			$keys = array_keys( $array );
			$newSanitizer = $sanitizer;
			if ( is_array( $sanitizer ) ) {
				foreach( $newSanitizer as $sanitizerKey => $value )
					if ( ! in_array( $sanitizerKey, $keys ) )
						unset( $newSanitizer[$sanitizerKey] );
			}

			foreach( $array as $arrayKey => $arrayValue ) {

				if ( is_array( $arrayValue ) ){
					$array[$arrayKey] = self::meta_box_array_map_r( $func, $arrayValue, $newSanitizer[$arrayKey] );
				}
			
				$array = array_map( $func, $array, $newSanitizer );
				$newMeta[$key] = array_combine( $keys, array_values( $array ) );
			}
		}

		return $newMeta;
	}

	
}
