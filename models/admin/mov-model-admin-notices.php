<?php

/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

class mov_Model_Admin_Notices extends mov_Model_Admin {

	const ADMIN_NOTICES_SETTINGS_NAME = 'admin_notices';
	const SETTINGS_NAME = mov_Core::mov_ID;
	protected static $settings;


	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	protected function __construct() { }


	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	public function register_hook_callbacks() {}


	/** 
	 * Show admin notices if plugin activation had any error
	 *
	 * @since    1.0.0
	 */
	public static function show_admin_notices() {

		$admin_notices = static::get_admin_notices();

		if ( ! empty( $admin_notices ) ) {

			foreach ( $admin_notices as $admin_notice ) {
				echo $admin_notice;
			}
			static::remove_admin_notices();

		}

	}


	/** 
	 * Helper to add Plugin Admin Notices
	 *
	 * @since    1.0.0
	 */
	public static function add_admin_notice( $notice ) {

		$admin_notices = static::get_admin_notices();

		if ( empty( $admin_notices ) || ! is_array( $admin_notices ) ) {
			$admin_notices = array();
		}

		$admin_notices[] = $notice;

		$settings = self::get_settings();
		$settings[static::ADMIN_NOTICES_SETTINGS_NAME] = $admin_notices;

		return static::update_settings( $settings );
	}


	/** 
	 * Helper to remove all Plugin Admin Notices
	 *
	 * @since    1.0.0
	 */
	public static function remove_admin_notices() {

		$admin_notices = static::get_admin_notices();
		if ( ! empty( $admin_notices )  ) {

			return static::delete_settings( static::ADMIN_NOTICES_SETTINGS_NAME );

		}

		return true;
	}


	/** 
	 * Helper to get Plugin Admin Notices
	 *
	 * @since    1.0.0
	 */
	private static function get_admin_notices() {

		$admin_notices = self::get_settings( static::ADMIN_NOTICES_SETTINGS_NAME );
		if ( ! empty( $admin_notices ) ) {
			return $admin_notices;
		}

		return false;
	}


	/**
	 * Retrieves all of the settings from the database
	 *
	 * @since    1.0.0
	 * @return array
	 */
	public static function get_settings( $setting_name = false ) {

		if ( ! isset( static::$settings ) ) {
			static::$settings = get_option( static::SETTINGS_NAME, array() );
		}

		if ( $setting_name ) {
			return isset( static::$settings[$setting_name] ) ? static::$settings[$setting_name] : array();
		}

		return static::$settings;

	}


}