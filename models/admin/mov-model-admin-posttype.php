<?php

/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

/**
* Register post type for movie
*/
class mov_Model_Admin_Posttype extends mov_Model_Admin {

    /**
     * Post Type Name
     *
     * @since   1.0.0
     */
    protected $post_type_name;

    /**
     * Holds the singular name of the post type.
     *
     * @since   1.0.0
     */
    protected $singular;

    /**
     * Holds the plural name of the post type.
     *
     * @since   1.0.0
     */
    protected $plural;

    /**
     * Custom Post Type registration labels.
     *
     * @since   1.0.0
     */
    protected $labels;

    /**
     * Additional settings for post type registration.
     *
     * @since   1.0.0
     */
    protected $settings;

    /**
     * Constructor
     * 
     * 
     * @since   1.0.0
     */
    public function __construct() {
    	$this->register_hook_callbacks();
    }


	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	public function register_hook_callbacks() {
        add_action( 'init', array( $this, 'register' ), 20 );
	}	

    /**
     * Add a new Custom Post Type
     *                                                 
     * @return  void
     */
    public function add( $post_type_names, $settings = array() ) {
        // Bail if the post type name hasn't been set
        if ( !isset( $post_type_names ) )
            return;
        $this->set_post_type_names( $post_type_names );
        $this->labels   = $this->set_labels();
        $this->settings = $this->set_settings( $settings );
    }


    /**
     * Register the Custom Post Type
     * 
     * @since   1.0.0
     */
    public function register() {
        // Array of the labels and settings for the CPT
        $args = array_merge( $this->settings, $this->labels );
        // Register our new custom post type
        register_post_type( $this->post_type_name, $args );
        flush_rewrite_rules();
    }


    /**
     * Assign the Custom Post Type registration settings
     * 
     * @return  array
     */
    protected function set_settings( $settings = array() ) {
        // Set the post type to public by default
        $defaults = array(
            'public' => true,
        );
        return array_replace_recursive( $defaults, $settings );
    }


    /**
     * Set the Custom Post Type labels.
     * 
     * @return  array       $labels     Post Type labels
     */
    protected function set_labels() {
        $singular = $this->singular;
        $plural   = $this->plural;
        $labels = array( 'labels' => array(
                'name'               => sprintf( __( '%s', 'textdomain' ), $plural ),
                'singular_name'      => sprintf( __( '%s', 'textdomain' ), $singular ),
                'menu_name'          => sprintf( __( '%s', 'textdomain' ), $plural ),
                'all_items'          => sprintf( __( '%s', 'textdomain' ), $plural ),
                'add_new'            => __( 'Add New', 'textdomain' ),
                'add_new_item'       => sprintf( __( 'Add New %s', 'textdomain' ), $singular ),
                'edit_item'          => sprintf( __( 'Edit %s', 'textdomain' ), $singular ),
                'new_item'           => sprintf( __( 'New %s', 'textdomain' ), $singular ),
                'view_item'          => sprintf( __( 'View %s', 'textdomain' ), $singular ),
                'search_items'       => sprintf( __( 'Search %s', 'textdomain' ), $plural ),
                'not_found'          => sprintf( __( 'No %s found', 'textdomain' ), $plural ),
                'not_found_in_trash' => sprintf( __( 'No %s found in Trash', 'textdomain' ), $plural ),
                'parent_item_colon'  => sprintf( __( 'Parent %s:', 'textdomain' ), $singular )
            ) );
        return $labels;
    }


    /**
     * Set the Custom Post Type names
     *
     * @param   string|arra
     */
    protected function set_post_type_names( $post_type_names ) {
        // Check if all the post_type_names have been supplied
        if ( is_array( $post_type_names ) ) {
            // Set the base post type name
            $this->post_type_name = $post_type_names[ 'post_type_name' ];
            $name_types = array( 'singular', 'plural' );
            // Loop through types of names and assign the correct value
            foreach ( $name_types as $name ) {
                // If the type has been set by the user
                if ( isset( $post_type_names[ $name ] ) ) {
                    // Use that setting
                    $this->$name = $post_type_names[ $name ];
                } else {
                    // Otherwise set the names ourselves
                    switch ( $name ) {
                        case 'singular':
                            $this->singular = $this->get_singular();
                            break;
                        case 'plural':
                            $this->plural = $this->get_plural();
                            break;
                        default:
                            break;
                    }
                }
            }
        } else {
            $this->post_type_name = $post_type_names;
            $this->singular       = $this->get_singular();
            $this->plural         = $this->get_plural();
        }
    }


    /**
     * Get singular
     *
     * @return  string
     */
    protected function get_singular( $name = null ) {
        // If no name is passed the post_type_name is used.
        if ( !isset( $name ) ) {
            $name = $this->post_type_name;
            if ( substr( $name, -1 ) == "s" )
                $name = substr( $name, 0, -1 );
        }
        else {
            $name = $this->singular;
        }
        return $this->get_human_friendly( $name );
    }


    /**
     * Get plural
     *
     * @return  string
     */
    protected function get_plural( $name = null ) {
        // If no name is passed the post_type_name is used.
        if ( !isset( $name ) )
            $name = $this->get_singular( $this->post_type_name );
        // Return the plural name. Add 's' to the end.
        return $this->get_human_friendly( $name ) . 's';
    }


    /**
     * Get human friendly
     *
     * @return  string
     */
    protected function get_human_friendly( $name = null ) {
        // If no name is passed the post_type_name is used.
        if ( !isset( $name ) )
            $name = $this->post_type_name;
        // Return human friendly name.
        return ucwords( strtolower( str_replace( "-", " ", str_replace( "_", " ", $name ) ) ) );
    }
}
