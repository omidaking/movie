<?php

/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

/**
* Register taxonomies class
*/
class mov_Model_Admin_Taxonomies extends mov_Model_Admin {

    /**
     * Taxonomy Name
     * 
     * @since   1.0.0
     */
    protected $taxonomy_name;

    /**
     * Post Type Name
     * 
     * @since   1.0.0
     */
    protected $post_type_name;

    /**
     * Holds the singular name of the taxonomy. 
     *
     * @since   1.0.0
     */
    protected $singular;

    /**
     * Holds the plural name of the taxonomy.
     *
     * @since   1.0.0
     */
    protected $plural;

    /**
     * Taxonomy registration labels.
     *
     * @since   1.0.0
     */
    protected $labels;

    /**
     * Remaining arguments for Taxonomy registration.
     *
     * @since   1.0.0
     */
    protected $settings;


    /**
     * Constructor
     * 
     * @since   1.0.0
     */
    public function __construct() {
        $this->register_hook_callbacks();
    }


    /**
     * Register callbacks for actions and filters
     *
     * @since    1.0.0
     */
    public function register_hook_callbacks() {
        add_action( 'init', array( $this, 'register' ), 20 );
    }   


    /**
     * Add a Taxonomy to register
     * 
     * @return  void
     */
    public function add( $taxonomy_names, $post_type_name = null, $settings = array() ) {
        // Bail if the taxonomy_name hasn't been set
        if ( !isset( $taxonomy_names ) )
            return;
        $this->set_taxonomy_names( $taxonomy_names );
        $this->post_type_name = $post_type_name;
        $this->labels         = $this->set_labels();
        $this->settings       = $this->set_settings( $settings );
    }


    /**
     * Register the Taxonomy
     * 
     * @since   1.0.0
     */
    public function register() {
        // Array of the labels and settings for the CPT
        $args = array_merge( $this->settings, $this->labels );
        register_taxonomy( $this->taxonomy_name, $this->post_type_name, $args );
        // Better to be safe than sorry according to WP wiki
        if ( isset( $this->post_type_name ) )
            register_taxonomy_for_object_type( $this->taxonomy_name, $this->post_type_name );
    }


    /**
     * Assign the Taxonomy registration settings.
     * 
     * @return  array
     */
    protected function set_settings( $settings = array() ) {
        // Set the tax to show in the post type admin column by default
        $defaults = array(
            'show_admin_column' => true,
        );
        // Combine the default settings with the incoming settings and return
        return array_replace_recursive( $defaults, $settings );
    }


    /**
     * Set the taxonomy labels.
     * 
     * @return  array       $labels     Taxonomy labels
     */
    protected function set_labels() {
        $singular = $this->singular;
        $plural   = $this->plural;
        $labels = array( 'labels' => array(
                'name'               => sprintf( __( '%s', 'textdomain' ), $plural ),
                'singular_name'      => sprintf( __( '%s', 'textdomain' ), $singular ),
                'menu_name'          => sprintf( __( '%s', 'textdomain' ), $plural ),
                'all_items'          => sprintf( __( '%s', 'textdomain' ), $plural ),
                'add_new'            => __( 'Add New', 'textdomain' ),
                'add_new_item'       => sprintf( __( 'Add New %s', 'textdomain' ), $singular ),
                'edit_item'          => sprintf( __( 'Edit %s', 'textdomain' ), $singular ),
                'new_item'           => sprintf( __( 'New %s', 'textdomain' ), $singular ),
                'view_item'          => sprintf( __( 'View %s', 'textdomain' ), $singular ),
                'search_items'       => sprintf( __( 'Search %s', 'textdomain' ), $plural ),
                'not_found'          => sprintf( __( 'No %s found', 'textdomain' ), $plural ),
                'not_found_in_trash' => sprintf( __( 'No %s found in Trash', 'textdomain' ), $plural ),
                'parent_item_colon'  => sprintf( __( 'Parent %s:', 'textdomain' ), $singular ),
                'parent_item'        => sprintf( __( 'Parent %s', 'textdomain' ), $singular ),
            ) );
        return $labels;
    }


    /**
     * Set the Taxonomy names
     * 
     * @param   string|array
     */
    protected function set_taxonomy_names( $taxonomy_names ) {
        // Check if all the post_type_names have been supplied
        if ( is_array( $taxonomy_names ) ) {
            // Set the taxonomy name
            $this->taxonomy_name = $taxonomy_names[ 'taxonomy_name' ];
            $name_types = array( 'singular', 'plural', 'slug' );
            foreach ( $name_types as $name ) {
                // If the type has been set by the user
                if ( isset( $taxonomy_names[ $name ] ) ) {
                    // Use that setting
                    $this->$name = $taxonomy_names[ $name ];
                } else {
                    // Otherwise set the names ourselves
                    switch ( $name ) {
                        case 'singular':
                            $this->singular = $this->get_singular();
                            break;
                        case 'plural':
                            $this->plural = $this->get_plural();
                            break;
                        default:
                            break;
                    }
                }
            }
        } else {
            $this->taxonomy_name = $taxonomy_names;
            $this->singular      = $this->get_singular();
            $this->plural        = $this->get_plural();
        }
    }


    /**
     * Get singular
     *
     * @return  string
     */
    protected function get_singular( $name = null ) {
        // If no name is passed the post_type_name is used.
        if ( !isset( $name ) ) {
            $name = $this->taxonomy_name;
            if ( substr( $name, -1 ) == "s" )
                $name = substr( $name, 0, -1 );
        }
        else {
            $name = $this->singular;
        }
        return $this->get_human_friendly( $name );
    }


    /**
     * Get plural
     *
     * @return  string
     */
    protected function get_plural( $name = null ) {
        // If no name is passed the taxonomy_names is used.
        if ( !isset( $name ) )
            $name = $this->get_singular( $this->taxonomy_name );
        // Return the plural name. Add 's' to the end.
        return $this->get_human_friendly( $name ) . 's';
    }


    /**
     * Get human friendly
     *
     * @return  string
     */
    protected function get_human_friendly( $name = null ) {
        // If no name is passed the taxonomy_name is used.
        if ( !isset( $name ) )
            $name = $this->taxonomy_name;
        return ucwords( strtolower( str_replace( "-", " ", str_replace( "_", " ", $name ) ) ) );
    }
}
