<?php
/**
 * Add function to widgets_init that'll load our widget.
 */
add_action( 'widgets_init', 'example_load_widgets' );
function example_load_widgets() {
	register_widget( 'mov_Model_admin_widget' );
}

/**
 * Widget movie.
 *
 * @since 0.1
 */
class mov_Model_admin_widget extends WP_Widget {

    /**
     * Widget setup.
     */
    function mov_Model_admin_widget() {
    	/* Widget settings. */
    	$widget_ops = array( 'classname' => 'movie-widget-wrap', 'description' => __('Demonstrate movie post type actors', 'mov') );

    	/* Widget control settings. */
    	$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'movie-widget' );

    	/* Create the widget. */
    	$this->WP_Widget( 'movie-widget', __('Movie Widget', 'mov'), $widget_ops, $control_ops );
    }

    /**
     * How to display the widget on the screen.
     */
    function widget( $args, $instance ) {
    	extract( $args );

    	$show_actor = isset( $instance['show_actor'] ) ? $instance['show_actor'] : false;

    	echo $before_widget;

    	if (  !$show_actor ) {
    		return;
    	}

    	$actors = get_terms( 'actor' , 'get=all');
    	$out = '<div class="movie-widget-wrap">
    				<h3>'. __( 'Actors', 'mov' ) .'<h3>
    				<ul>';
    	foreach ( $actors as $key => $value ) {
    		$out .= '<li data-type="actor" data-actor="'.$value->slug.'">'. $value->name.'</li>';
    	}
    	$out .= '	</ul>
    			</div>';


    	echo $out;
    	echo $after_widget;
    }

    /**
     * Update the widget settings.
     */
    function update( $new_instance, $old_instance ) {
    	$instance = $old_instance;

    	$instance['show_actor'] = $new_instance['show_actor'];

    	return $instance;
    }

    /**
     * Displays the widget settings controls on the widget panel.
     */
    function form( $instance ) {

    	$defaults = array( 'show_actor' => true );
    	$instance = wp_parse_args( (array) $instance, $defaults ); ?>
    	<p>
    		<input class="checkbox" type="checkbox" <?php checked( $instance['show_actor'], true ); ?> id="<?php echo $this->get_field_id( 'show_actor' ); ?>" name="<?php echo $this->get_field_name( 'show_actor' ); ?>" /> 
    		<label for="<?php echo $this->get_field_id( 'show_actor' ); ?>"><?php _e('Enable actor widget??', 'mov'); ?></label>
    	</p>

    	<?php
    }
}

?>