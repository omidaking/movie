<?php

/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

class mov_Model_Public_Ajax extends mov_Model_Public {

	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {

		$this->register_hook_callbacks();

	}

	public function register_hook_callbacks() {

		$pcm = mov_Controller_Public_Movie_Filter::get_instance();

		if ( mov_Request_Validator::is_ajax() ) {

			// pcm
			mov_Actions_Filters::add_action( "wp_ajax_$pcm->ajax_action", $pcm,  'render' );
			mov_Actions_Filters::add_action( "wp_ajax_nopriv_$pcm->ajax_action",  $pcm, 'render' );


		}

	}

}