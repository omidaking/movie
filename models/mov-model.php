<?php

/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */


abstract class mov_Model {

	private static $instances = array();
	private static $core;
	private static $current_user;
	private $settings;
	protected $user_setting;

	/**
	 * Provides access to a single instance of a module using the singleton pattern
	 *
	 * @since    1.0.0
	 */
	public static function get_instance() {

		$classname = get_called_class();

		if ( ! isset( self::$instances[ $classname ] ) ) {
			self::$instances[ $classname ] = new $classname();
		}
		return self::$instances[ $classname ];

	}

	public static function get_core() {
		self::$core = new mov_Core;
		return self::$core;
	}

	public static function get_current_user() {
		
		self::$current_user = wp_get_current_user();
		return self::$current_user;

	}		

	/**
	* Get post id.
	*
	* @since    1.0.0
	*/
	public function post_id() {

		return get_the_ID();

	}

	/**
	 * Get model
	 *
	 * @since    1.0.0
	 */
	protected function get_user_settings() {
		$settings = new mov_Settings;
		return $settings->get_user_option();
	}


	/**
	 * Render a template
	 *
	 * @since    1.0.0
	 */
	protected static function render_template( $default_template_path = false, $variables = array(), $require = 'once' ){
		
		if ( ! $template_path = locate_template( basename( $default_template_path ) ) ) {
			$template_path =  mov_Core::get_mov_path() . '/views/' . $default_template_path;
		}

		if ( is_file( $template_path ) ) {
			extract( $variables );
			ob_start();
			if ( 'always' == $require ) {
				require( $template_path );
			} else {
				require_once( $template_path );
			}
			$template_content = apply_filters( 'mov_template_content', ob_get_clean(), $default_template_path, $template_path, $variables );
		} else {
			$template_content = '';
		}

		return $template_content;
	}
	


	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	abstract protected function __construct();

	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	abstract public function register_hook_callbacks();

}
