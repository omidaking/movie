<?php

/**
 *
 * 					  https://realtyna.com/
 * @since             1.0.0
 * @package           Movie
 *
 * Plugin Name:       Movie
 * Plugin URI:        https://realtyna.com/
 * Description:       Show movie and actor
 * Version:           1.0.0
 * Author:            Realtyna
 * Author URI:        https://realtyna.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mov
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
*  Run main steps for Movie
*/
class mov {
	
	/**
	 *  Php version required
	 */
	const  mov_PHP_VERSION = '5.3';

	/**
	 * Wp Version required
	 */
	const  mov_REQUIRED_WP_VERSION = '4.0';

	/**
	 * Activation Cunstructor
	 */
	public function __construct() {

		//Check requirements and load main class
		if ( $this->mov_requirements_needs() ) {

			require_once plugin_dir_path( __FILE__ ) . 'lib/mov-core.php';
			$plugin = mov_Core::get_instance();

		} else {

			add_action( 'admin_notices', array( &$this, 'mov_requirements_error' ) );
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			deactivate_plugins( plugin_basename( __FILE__ ) );

		}
	}

	/**
	 * Checks if the system requirements are met
	 *
	 * @since    1.0.0
	 * @return bool True if system requirements are met, false if not
	 */
	public function mov_requirements_needs() {

		global $wp_version;

		if ( version_compare( self::mov_PHP_VERSION ,  PHP_VERSION, '>' ) ) {
			return false;
		}

		if ( version_compare( $wp_version, self::mov_REQUIRED_WP_VERSION, '<' ) ) {
			return false;
		}

		return true;

	}

	/**
	 * Prints an error that the system requirements weren't met.
	 *
	 * @since    1.0.0
	 */
	public function mov_requirements_error() {

		global $wp_version;
		require_once( dirname( __FILE__ ) . '/views/admin/errors/requirements-error.php' );

	}
}

new mov;