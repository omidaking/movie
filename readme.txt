﻿=== Movie ===
Contributors: realtyna
Donate link: https://realtyna.com/
Tags: Movie
Requires at least: 3.6
Tested up to: 4.9.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==
Online demo : http://omidakhavan.ir/index.php/movie/

Import file for movie post type:
http://omidakhavan.ir/download/movie.xml

Import file for shortcodes post type:
http://omidakhavan.ir/download/shortcode_genrator.xml