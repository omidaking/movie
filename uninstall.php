<?php

/**
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Movie
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

function mov_unistall() {
	require_once ( plugin_dir_path( __FILE__ ) . 'lib/mov-loader.php' );
	mov_Loader::uninstall_plugin();
}

mov_unistall();