(function( $ ) {
	'use strict';

	/**
     *  Start mov backend 
     */
	$(document).ready(function($) {
		// click on shortcodes
		$(document).on( 'change', ".movie-search-bar select", function(e){
			e.preventDefault();

			// get type of select box 
			var $this = $(this) , type = $(this).find(':selected').attr('data-type') , value = $(this).find(':selected').val();

			$('.mov-preloader-wrap').css('display', 'block');

			$.ajax( {
				type: "POST",
				url: mov.adminajax,
				data: {
					action   : 'movie',
					security : mov.nounce,
					type : type,
					value : value
				},
			}).done( function(data) {
				console.log(data);
				$('.mov-preloader-wrap').css('display', 'none');
				$('.movie-inner-wrap').remove();
				$(data).insertAfter('.movie-search-bar');

			}).fail(function(){
				$('.mov-preloader-wrap').css('display', 'none');
			});

		});	
		// widget
		$(document).on( 'click', ".movie-widget-wrap ul li", function(e){
			e.preventDefault();

			// get type of select box 
			var $this = $(this) , type = $(this).attr('data-type'), value = $(this).attr('data-actor');
			$.ajax( {
				type: "POST",
				url: mov.adminajax,
				data: {
					action   : 'movie',
					security : mov.nounce,
					k : 'widget',
					type : type,
					value : value
				},
			}).done( function(data) {
				$('.movie-widget-inner').remove();
				$(data).insertAfter('.movie-widget-wrap ul');

			}).fail(function(){
			});

		});	

	});
	/**
     *  End mov backend 
	*/

})( jQuery );
